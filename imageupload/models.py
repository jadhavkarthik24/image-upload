from django.db import models

# Create your models here.
class Images(models.Model):
    image_id  = models.AutoField(primary_key=True)  
    image_url = models.URLField(max_length=512,null=True)
    image_shorten_url = models.CharField(max_length=512,null=True)
    img_password = models.CharField(max_length=512,null=True)
    img_views = models.IntegerField(default=0)
    upload_datetime = models.DateField()

    def __str__(self):
        return str(self.image_id)