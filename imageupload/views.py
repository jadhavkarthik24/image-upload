import os
from .models import *
from datetime import datetime
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render,redirect
from django.contrib.auth.hashers import make_password, check_password
# import hashlib
from django.core.files.storage import FileSystemStorage

# Create your views here.
if os.environ['HOME'] == '/home/jkd':
    BASE_URL = "http://127.0.0.1:8000"
else:
    BASE_URL = "http://dharavathkarthik.pythonanywhere.com"


def ImageUpload(request):
    """ fetching all image details """
    images = Images.objects.values(
        'image_shorten_url', 'img_views', 'upload_datetime').order_by('img_views').reverse()
    if request.method == 'POST' and request.FILES['imagefile']:
        # print(request.path)
        """ Upload image """
        myfile = request.FILES['imagefile']
        pwd = request.POST.get('pwd', '')
        if pwd != "":
            password = make_password(pwd)
        else:
            password = ""
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        if os.environ['HOME'] == '/home/jkd':
            Images.objects.create(
                image_url="http://localhost:8000{}".format(uploaded_file_url),
                upload_datetime=datetime.now(),
                img_password=password
            )
        else:
            Images.objects.create(
                image_url=BASE_URL+uploaded_file_url,
                upload_datetime=datetime.now(),
                img_password=password
            )
        last_id = Images.objects.latest('image_id')
        shorten_url = BASE_URL+"/"+str(last_id.image_id)
        image = Images.objects.get(image_id=last_id.image_id)
        image.image_shorten_url = shorten_url
        image.save()

        return render(request, 'index.html', {
            'uploaded_file_url': shorten_url,
            'images': images
        })
    return render(request, 'index.html', {'images': images})


def fetch_image(request, image_id):
    image = Images.objects.get(image_id=image_id)
    image_url = image.image_url
    print(image_url)
    if image.img_password != "":
        if request.method == 'POST':
            pwd = request.POST.get('pwd', '')
            print(pwd)
            if pwd == "":
                return render(request, 'error.html', {'message': 'Please enter password'})
            if check_password(pwd, image.img_password):
                image.img_views += 1
                image.save()
                return HttpResponseRedirect(image_url)
            else:
                return render(request, 'error.html', {'message': 'Unautorized Access'})
        return render(request, 'passwordcheck.html')
    else:
        image.img_views += 1
        image.save()
        return HttpResponseRedirect(image_url)
